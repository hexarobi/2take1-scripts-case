# 2take1-scripts-case

My scripts primarily for other developers and users with 2Take1.

# INI Parser
INI Parser (`ini_parser.lua`) is a modular utility compatible with the 2Take1 Lua API. It features:
  - A simple object-oriented approach to configuration IO.
  - Significantly better readability as opposed to base Lua IO. 
  - A collection of some neat and small additions that extend the avaliable IO.
    - One example of this is added support for relative imports for `ini.parse`. It's not huge, just neat.
  - Probably pretty solid performance, INI files will rarely reach lengths that'll be seriously problematic.
  - Guarenteed maintenance. New scripts always have the unknown of long-term reliability. As long as people use this, it'll stay updated. 
    - If awhile passes without an update, then it appears I finally achieved bug-free status. Now, post a suggestion for me :)

## INI Parser Information
- `function ini.parse(path, cwd)` — Parses an INI file @ cwd .. path ::
  - `cwd` is an optional parameter to define a CWD other than `2Take1Menu\\scripts\\`. It must be an absolute path.
  - **Returns:**
    - Table:
      - `function Table.save(_path, skip_keys)`
        - Saves this INI configuration. 
          - `_path` is an optional path to specify an alternative file to save as. It needs to be absolute.
          - `skip_keys` is an array of keys to not save.
<br/><br/>
- `function ini._Case_Ini_Recurse(tab, tab_name)` — [DEBUG]: Recursively prints the contents of a table ::
  - `tab` specifies the table to recurse upon.
  - `tab_name` is an optional parameter to specify the name of the table being recursed upon.

### INI Parser Usage
Example 1:
```lua
local ini = require "./ini_parser"
local cfg = ini.parse "my_ini.ini"

local function input(msg)
    local a 
    repeat 
        io.write(msg)
        io.flush()
        a = io.read()
    until a

    return a
end

cfg.username = input "What is your name?"
cfg.color.fav = input "On a side note, what's your favorite color?"
cfg.other.age = input "Also, what is your age?"
cfg.save()
```
```ini
; my_ini.ini — The simple script above produces the following result:
username=Ryan
[color]
fav=red
[other]
age=99
```
Example 2:
```lua
local ini = require "./ini_parser"
local cfg = ini.parse "my_ini.ini"

print("Hello there, " .. cfg.username .. "!\n")
print("Your favorite color is " .. cfg.color.fav .. ", and you're " .. tostring(cfg.other.age) .. "!")
```

#### INI Parser, Further Information
- Booleans and numbers are automatically converted into Lua values from INI. 
- If a key can't be accessed syntacticly (i.e, like.this), use["this format"].
- Spaces trailing your section \[brackets] will prevent it from being registered. 

# Suggestions
- I am open to suggestions of improvements or scripts. Create an issue and I'll respond in a timely manner. 
